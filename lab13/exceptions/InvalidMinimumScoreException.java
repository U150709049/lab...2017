package exceptions;

public class InvalidMinimumScoreException extends Exception {

	public InvalidMinimumScoreException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidMinimumScoreException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidMinimumScoreException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidMinimumScoreException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidMinimumScoreException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
