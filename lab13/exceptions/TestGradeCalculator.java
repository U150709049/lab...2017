package exceptions;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class TestGradeCalculator {

	public static void main(String[] args) {
		
		GradeCalculator calc = new GradeCalculator();
		Scanner scanner = new Scanner(System.in);
		
		
		//Request minimum scores for grades
		while (!calc.minimumScoresCompleted()){
			String grade = calc.nextGradeWithoutScore();
			System.out.println("Enter minimum score for " + grade + ":");
			
			String minScore = scanner.nextLine();
			try {
				calc.setMinScore(grade,minScore);
			} catch (InvalidMinimumScoreException e) {
				System.out.println(e.getMessage());
			}
		}
		
	
		
		String fileName = null;
		
		while(true){
			try {
				System.out.print("Enter input file:");
				fileName = scanner.nextLine();
				calc.printGrades(fileName);
				break;
			} catch (FileNotFoundException e) {
				System.out.println("Can not find file " + fileName);
			}
		}
		
		scanner.close();

	}

}
