public class Question7 {
	public static String hexa(int n){
		if(n==15){
			return "F";
		}
		else if(n==14){
			return "E";
		}
		else if(n==13){
			return "D";
		}
		else if(n==12){
			return "C";
		}
		else if(n==11){
			return "B";
		}
		else if(n==10){
			return "A";
		}
		else if(n<10){
			return ""+n;
		}
		return hexa(n/16)+hexa(n%16);
	}
	public static void main(String []args ){
		System.out.println(hexa(175));
		
	}

}
