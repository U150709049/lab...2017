package drawing;

public abstract class Shape {
	public abstract double area();
	public abstract void draw();
	public abstract void move(int xDistance,  int yDistance);
	
		
	}


